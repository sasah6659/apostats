import json
import JsonManager
import RequestManager
import asyncio
import GoogleSheet
from datetime import datetime

class Player:
    def __init__(self, summoner_name, date, champion, kills=0, deaths=0, assists=0, win_or_lose=None, ward=None, pink=None, vision_score=0, farm=0, game_time=0):
        self.summoner_name = summoner_name
        self.date = date
        self.champion = champion
        self.kills = kills
        self.deaths = deaths
        self.assists = assists
        self.win_or_lose = win_or_lose
        self.ward = ward
        self.pink = pink
        self.vision_score = vision_score
        self.farm = farm
        self.game_time = game_time


    def __str__(self):
        attributes = [
            f"Date: {self.date}",
            f"Champion: {self.champion}",
            f"Kills: {self.kills}",
            f"Deaths: {self.deaths}",
            f"Assists: {self.assists}",
            f"Win or Lose: {self.win_or_lose}",
            f"Ward: {self.ward}",
            f"Pink: {self.pink}",
            f"Vision Score: {self.vision_score}",
            f"Farm: {self.farm}",
            f"Game Time: {self.game_time}",
        ]
        return "\n".join(attributes)

async def GetAllPlayersAsync(playersName):
    #Async method which will get all players matches
    async with asyncio.TaskGroup() as tg:
        for player_name in playersName:
            tg.create_task(GetPlayerMatchDetailAsync(player_name))
    print("all task completed")

async def GetPlayerMatchDetailAsync(player_name) :
    googleSheet = GoogleSheet.GoogleSheet()
    summoner = RequestManager.GetSummoner(player_name)    
    if(summoner == None):
        return
    history = RequestManager.GetPlayerHistory(summoner.get('puuid'))
    if(history == None):
        return
    index = 0
    for game_Id in history:
        if(JsonManager.CheckGameDataAlreadyExist(player_name, game_Id)):
            continue
        index = index+1
        game = RequestManager.GetGameDetails(game_Id)
        playerList = SetGameInfo(player_name, game_Id, game)
        playerNames = JsonManager.LoadPlayersName()
        players = [player for player in playerList if player_name in player.summoner_name]
        for p in players:
            googleSheet.addInfoLine(playerNames[p.summoner_name], index, [value for key, value in p.__dict__.items() if key != 'summoner_name'])

    print(f"\n {player_name} \n Task Complete \n")

def SetGameInfo(playerGames_name, game_Id, game):
    if 'info' in game:
        game_info = game['info']
        listOfPlayers = []

        for participant in game_info['participants']:
            player_name = participant['summonerName']
            date = datetime.utcfromtimestamp(game_info['gameCreation']/1000).isoformat()
            champion = participant['championName']
            kills = participant['kills']
            deaths = participant['deaths']
            assists = participant['assists']
            win_or_lose = "WIN" if participant['win'] else "LOSE"
            ward = participant['wardsKilled'] + participant['wardsPlaced']
            pink = participant['visionWardsBoughtInGame']
            vision_score = participant['visionScore']
            farm = participant['totalMinionsKilled'] + participant['neutralMinionsKilled']
            game_time = game_info['gameDuration']/60

            listOfPlayers.append(Player(
                player_name, date, champion, kills, deaths, assists, win_or_lose, ward, pink, vision_score, farm, round(game_time)
            ))
        jsonSerialized = json.dumps([player.__dict__ for player in listOfPlayers], indent=0)
        JsonManager.SaveGameData(playerGames_name, game_Id, jsonSerialized)
        return listOfPlayers
    else:
        print(f"Game info not found for game ID {game_Id}")

# Main Logic
playersName = JsonManager.LoadPlayersName()
asyncio.run(GetAllPlayersAsync(playersName))
    
