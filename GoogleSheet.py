import gspread
import time
from oauth2client.service_account import ServiceAccountCredentials


class GoogleSheet:
    def __init__(self, json_path = 'apoteamsync.json'):
        credentials = ServiceAccountCredentials.from_json_keyfile_name('apoteamsync.json', ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive'])
        self.auth = gspread.authorize(credentials)

    # need opti to only push the full matrix in one push and not multiple line push
    # I am lazy :(
    def addInfoLine(self, player_role, index, values):
        sh = self.auth.open_by_key('1e5spO6LRgrvYo-x5OjGAoqGK2iJa6tgBlRL6mh7eL3c')
        worksheet = sh.worksheet(player_role)
        cell_list = worksheet.range(f'B{2+index}:L{2+index}')
        if len(values) != len(cell_list):
            print("Number of values does not match the number of cells to update.")
            return
        for i, cell in enumerate(cell_list):
            cell.value = values[i]
        time.sleep(3)
        worksheet.update_cells(cell_list)