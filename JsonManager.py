import PathManager
import os
import json

def CreateDirectoryIfNotExist(path):
     if(os.path.isdir(path) == False):
          os.mkdir(path)

def CheckGameDataAlreadyExist(player_name, game_Id):
    GameDataPath = PathManager.GetPlayerGameFilePath(player_name, game_Id)
    return os.path.isfile(GameDataPath)

#region Load Data

def LoadPlayersName():
    """
    Load players name which be searched
    """
    path = PathManager.PlayersName

    if(os.path.isfile(path)):
        with open(path) as file:
            data = json.load(file)        
            return data
    else :
        print("File was not created, an empty file has been created")
        CreateDirectoryIfNotExist(PathManager.EntryData)
        data = ""
        with open(path, "w") as file:
             file.write(data)
         
    
#endregion


#region Save Data

def SaveGameData(player_name, game_Id, json):
    """
    Save a match data
    """
    GameDataPath = PathManager.GetPlayerGameFilePath(player_name, game_Id)
    if(os.path.isfile(GameDataPath) == False):
        #Create All Directory if needed
        CreateDirectoryIfNotExist(PathManager.DataPath)
        CreateDirectoryIfNotExist(PathManager.GetPlayerPath(player_name))
        CreateDirectoryIfNotExist(PathManager.GetPlayerGamesPath(player_name))
    
    #Create File
    with open(GameDataPath, "w") as f:
                f.write(json)

def SaveExcel(excelFile):
    CreateDirectoryIfNotExist(PathManager.Stats)
    excelFile.save(f"{PathManager.Stats}statistiques_joueurs.xlsx")

#endregion