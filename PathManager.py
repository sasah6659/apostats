#region Entry Data

EntryData = "EntryData/"
PlayersName = f"{EntryData}PlayersName.json"

#endregion


#region DataCollected

DataPath = "DataCollected/"
GamesPath = "Games/"

Stats = "Stats/"

def GetPlayerPath(player_name):
    """
    Return path for specific player
    takes a player_name  as argument
    """
    return f"{DataPath}{player_name}/"

def GetPlayerGamesPath(player_name):
    """
    Return path of folder that contains all Games of player
    takes a player_name and game_Id as argument
    """
    return f"{GetPlayerPath(player_name)}{GamesPath}"

def GetPlayerGameFilePath(player_name, game_Id):
    """
    Return path of a specific game
    takes a player_name and game_Id as argument
    """
    return f"{GetPlayerGamesPath(player_name)}gameData-{game_Id}.json"

#endregion

