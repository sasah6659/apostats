from riotwatcher import LolWatcher, ApiError

# Constantes
#API_KEY = 'RGAPI-cb3badd6-5eeb-4258-ae1b-a81ca9df6bcd'
API_KEY = 'RGAPI-cfa4d117-6557-4a41-b2b4-52f68fefb306'
REGION = 'euw1'
QUEUE = 420  # 'RANKED_SOLO_5x5'

Watcher = LolWatcher(API_KEY)

def GetSummoner(player_name):
    try:
        summoner = Watcher.summoner.by_name(REGION, player_name)
        return summoner
    except ApiError as err:
        RaiseError(err)
        return None


def GetPlayerHistory(puuid):
    try:
        history = Watcher.match.matchlist_by_puuid(REGION, puuid, count=20, queue= QUEUE)
        return history
    except ApiError as err:
        RaiseError(err)
        return None

def GetGameDetails(game_Id):
    try:
        game = Watcher.match.by_id(REGION, game_Id)
        return game
    except ApiError as err:
        RaiseError(err)
        return None

def RaiseError(error):
    if error.response.status_code == 429:
        print(f'We should retry in {error.response.headers["Retry-After"]} seconds.')
        print('This retry-after is handled by default by the RiotWatcher library.')
        print('Future requests wait until the retry-after time passes.')
    elif error.response.status_code == 404:
        print('Summoner with that ridiculous name not found.')
    else:
        raise