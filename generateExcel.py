import pandas as pd
import os
from pathlib import Path
from openpyxl import Workbook
from openpyxl.chart import LineChart, Reference, BarChart3D
import JsonManager
import PathManager

# Crée un classeur Excel

def CreateExcel(joueurs) : 
    wb = Workbook()

    # Dictionnaire pour stocker les moyennes de farm_per_minute
    moyennes = {}

    for joueur in joueurs:
        df = pd.DataFrame()
        joueur_count = 0  # Pour le débogage, compteur des parties du joueur en cours

        # Obtenez le chemin du répertoire du joueur
        path = PathManager.GetPlayerGamesPath(joueur)
        for file in os.listdir(path):
            if file.endswith(".json"):
                chemin_fichier = os.path.join(path, file)
                df_temp = pd.read_json(chemin_fichier)
                
                # Débogage : comptez les parties du joueur en cours
                if df_temp['summoner_name'].str.contains(joueur).any():
                    joueur_count += 1

                df_temp_nonono = df_temp[df_temp['summoner_name'] == joueur]

                df = pd.concat([df, df_temp_nonono], ignore_index=True)

        # Débogage : Affiche le nombre de parties du joueur en cours
        print(f"Nombre de parties de {joueur} : {joueur_count}")

        df['farm_per_minute'] = round(df['farm'] / (df['game_time']), 1)

        ws = wb.create_sheet(title=joueur)  # Crée une feuille pour le joueur

        headers = list(df.columns)
        ws.append(headers)
        headers = list(df.columns)
        ws.append(headers)

        for r_idx, row in enumerate(df.iterrows(), start=2):
            index, data = row
            ws.append(data.tolist())
        for r_idx, row in enumerate(df.iterrows(), start=2):
            index, data = row
            ws.append(data.tolist())

        # Calcul des moyennes de farm_per_minute pour chaque joueur
        moyenne_farm_per_minute = round(df['farm_per_minute'].mean(), 1)
        moyennes[joueur] = moyenne_farm_per_minute

    # Crée un onglet "stats"
    ws_stats = wb.create_sheet(title="Stats")

    # Écrit les moyennes dans l'onglet "stats"
    ws_stats.append(["Joueur", "Moyenne Farm per Minute"])
    for joueur, moyenne in moyennes.items():
        ws_stats.append([joueur, moyenne])

    # Crée un graphique pour visualiser les moyennes (BarChart3D)
    chart_stats = BarChart3D()
    chart_stats.title = "Comparaison de la Moyenne Farm per Minute"
    chart_stats.y_axis.title = "Farm per Minute"
    chart_stats.x_axis.title = "Joueur"

    # Référence pour les données de farm des joueurs
    data_stats = Reference(ws_stats, min_col=2, min_row=1, max_row=len(joueurs) + 1, max_col=2)
    chart_stats.add_data(data_stats, titles_from_data=True)

    # Définir les noms des joueurs comme catégories
    categories_stats = Reference(ws_stats, min_col=1, min_row=2, max_row=len(joueurs) + 1, max_col=1)
    chart_stats.set_categories(categories_stats)

    ws_stats.add_chart(chart_stats, "K2")

    # Supprime la feuille par défaut
    del wb["Sheet"]

    # Enregistre le classeur Excel
    JsonManager.SaveExcel(wb)
    print("Excel correctement généré")

# Main Logic 
playersName = JsonManager.LoadPlayersName()
CreateExcel(playersName)
