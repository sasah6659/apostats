from riotwatcher import LolWatcher, ApiError

api_key = 'RGAPI-cb3badd6-5eeb-4258-ae1b-a81ca9df6bcd'
my_region = 'euw1'
player_name = 'Finary'

watcher = LolWatcher(api_key)

try:
    player = watcher.summoner.by_name(my_region, player_name)
    puuid = player.get('puuid')
    print(f"Le puuid de {player_name} est : {puuid}")
except ApiError as err:
    if err.response.status_code == 429:
        print('Nous devrions réessayer dans {} secondes.'.format(err.response.headers['Retry-After']))
    elif err.response.status_code == 404:
        print(f"Joueur avec le nom d'invocateur '{player_name}' introuvable.")
    else:
        raise
