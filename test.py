import pandas as pd
import os
from openpyxl import Workbook
from openpyxl.chart import LineChart, Reference

# Initialisez un DataFrame vide
df = pd.DataFrame()

# Parcourez tous les fichiers JSON dans le répertoire actuel
for fichier in os.listdir():
    if fichier.endswith(".json"):
        # Lisez le fichier JSON en tant que DataFrame
        df_temp = pd.read_json(fichier)
        
        # Filtrer les lignes pour obtenir seulement les données du joueur "APO Nono"
        df_temp_nonono = df_temp[df_temp['summoner_name'] == 'Kovα']
        
        # Ajoutez le DataFrame filtré au DataFrame principal
        df = pd.concat([df, df_temp_nonono], ignore_index=True)

# Calculez la moyenne du farm par minute
df['farm_per_minute'] = round(df['farm'] / (df['game_time']),1)  # Calcul de la moyenne du farm par minute




# Créez un fichier Excel
wb = Workbook()
ws = wb.active

# Ajoutez les en-têtes de colonnes
headers = list(df.columns)
ws.append(headers)

# Ajoutez les données du DataFrame au fichier Excel
for r_idx, row in enumerate(df.iterrows(), start=2):
    index, data = row
    ws.append(data.tolist())

# Créez un graphique de ligne pour la moyenne du farm par minute
chart = LineChart()
chart.title = "Moyenne du Farm par Minute"
chart.y_axis.title = "Farm par Minute"
chart.x_axis.title = "Date"


data = Reference(ws, min_col=13, min_row=2, max_row=len(df) + 1, max_col=13)
categories = Reference(ws, min_col=2, min_row=2, max_row=len(df) + 1, max_col=2)

chart.add_data(data, titles_from_data=True)
chart.set_categories(categories)

ws.add_chart(chart, "L2")  # Ajoutez le graphique à la feuille Excel

# Écrivez le DataFrame filtré dans un fichier Excel
wb.save("statistiques_NONO.xlsx")
